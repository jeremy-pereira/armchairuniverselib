import XCTest
import ArmchairUniverseLib
@testable import FlibLib

final class FlibLibTests: XCTestCase
{
    func testExample()
	{
		let       sequence = [0, 1, 1, 1, 0, 0, 0, 0, 1, 0, 1, 1, 0]
		let expectedOutput = [1, 0, 0, 0, 0, 1, 1, 0, 0, 1, 0, 0, 0]
		let flib = Flib(chromosome: [1, 1, 1, 2,
									 0, 2, 0, 1,
									 1, 0, 0, 0])
		let prediction = flib.predict(sequence)
		XCTAssert(prediction == expectedOutput, "Failed prediction:\nP=\(prediction)\nE=\(expectedOutput)")
    }

	func testCrossOver()
	{
		let chromA = [Int](repeating: 1, count: 10)
		let chromB = [Int](repeating: 0, count: 10)

		let result1 = chromA.cross(with: chromB, start: 3, end: 8)
		XCTAssert(result1 == [1, 1, 1, 0, 0, 0, 0, 0, 1, 1])
		let result2 = chromA.cross(with: chromB, start: 8, end: 3)
		XCTAssert(result2 == [0, 0, 0, 1, 1, 1, 1, 1, 0, 0])
	}

	func testBreed()
	{
		// Random values calculated to give us 3 and 8 when we get the random
		// start and end
		var rng = DeterministicNumberGenerator([(3 + 1) * (UInt64.max / 16),
												(8 + 1) * (UInt64.max / 16)])
		let flib1 = Flib(chromosome: [1, 0, 1, 1,
									  0, 3, 1, 0,
									  0, 2, 1, 3,
									  1, 1, 0, 2])
		let flib2 = Flib(chromosome: [0, 3, 0, 2,
									  0, 3, 0, 1,
									  1, 0, 0, 2,
									  1, 1, 1, 0])
		let offspring = flib1.breed(with: flib2, using: &rng)
		XCTAssert(offspring.chromosome == [1, 0, 1, 2,
										   0, 3, 0, 1,
										   0, 2, 1, 3,
										   1, 1, 0, 2], "Got \(offspring.chromosome)")
	}

    static var allTests =
	[
        ("testExample", testExample),
		("testCrossOver", testCrossOver),
    ]
}
