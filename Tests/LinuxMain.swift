import XCTest

import ArmchairUniverseLibTests

var tests = [XCTestCaseEntry]()
tests += ArmchairUniverseLibTests.allTests()
tests += FlibLibTests.allTests()
XCTMain(tests)
