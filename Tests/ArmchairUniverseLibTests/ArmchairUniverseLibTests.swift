//
//  ArmchairUniverseLibTests.swift
//  ArmchairUniverseLibTests
//
//  Created by Jeremy Pereira on 31/08/2019.
//  Copyright © 2019 Jeremy Pereira. All rights reserved.
//

import XCTest
@testable import ArmchairUniverseLib

class ArmchairUniverseLibTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testDeterministicNumberGenerator()
	{
		var dng = DeterministicNumberGenerator([0, 1, 3])
		XCTAssert(dng.next() == UInt64(0))
		XCTAssert(dng.next() == UInt64(1))
		XCTAssert(dng.next() == UInt64(3))
		XCTAssert(dng.next() == UInt64(0))
    }


	/// Check our ssumptions about how random numbers are generated are
	/// correct.
	func testDeterministicResults()
	{
		let numbersRequired = UInt64(0) ..< 16
		let randomValues = numbersRequired.map { ($0 + 1) * (UInt64.max / 16)  }
		var rng = DeterministicNumberGenerator(randomValues)
		for n in numbersRequired
		{
			let r = Int.random(in: 0 ..< 16, using: &rng)
			XCTAssert(r == n, "r = \(r), n = \(n)")
		}
	}
}
