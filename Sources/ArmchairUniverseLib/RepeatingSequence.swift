//
//  RepeatingSequence.swift
//  
//
//  Created by Jeremy Pereira on 23/08/2020.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.


/// A sequence that repeats an array of values a finite number of times
public struct RepeatingSequence<Element>
{
	fileprivate let maxRepeats: Int
	fileprivate let elements: [Element]
	/// Initialise the sequence from an array
	/// - Parameters:
	///   - maxRepeats: Number of times the sequence will repeat
	///   - array: The items to repeat
	public init(maxRepeats: Int, array: [Element])
	{
		self.elements = array
		self.maxRepeats = maxRepeats
	}
}

extension RepeatingSequence: Sequence
{
	public struct Iterator: IteratorProtocol
	{
		private let parent: RepeatingSequence<Element>
		private var index = 0
		private var repeats = 0

		fileprivate init(parent: RepeatingSequence<Element>)
		{
			self.parent = parent
		}

		public mutating func next() -> Element?
		{
			guard repeats < parent.maxRepeats else { return nil }
			let ret = parent.elements[index]
			index += 1
			if index == parent.elements.count
			{
				index = 0
				repeats += 1
			}
			return ret
		}
	}

	public func makeIterator() -> Iterator
	{
		return Iterator(parent: self)
	}
}
