//
//  TypeExtensions.swift
//  ArmchairUniverseLib
//
//  Created by Jeremy Pereira on 02/09/2019.
//  Copyright © 2019 Jeremy Pereira. All rights reserved.
//

public extension Int
{
	/// Return the mathematical modulus of an integer
	///
	/// This differs from `%` in that the result is always positive.
	/// - Parameter x: Number for the modulus
	/// - Returns: The mathematical modulus of `self`
	func modulus(_ x: Int) -> Int
	{
		let remainder = self % x
		return remainder >= 0 ? remainder : abs(x) + remainder
	}
}

/// Marker for code that is not yet implemented
///
/// If no parameters are filled in, you'll get a message like this:
/// ```
/// file.swift(123): someFunc not implemented ''
/// ```
///
/// - Parameters:
///   - message: Message to displayed if this function is ever executed.
///   - file: The file in which the call occurred - defaults to current file
///   - line: The line number to be reported - defaults to the line of rthe function call
///   - function: The function to be reported - defaults to the function we are in
/// - Returns: never retrurns
public func notImplemented(_ message: String = "", file: String = #file, line: Int = #line, function: String = #function) -> Never
{
	fatalError("\(file)(\(line)): \(function) not implemented, '\(message)'")
}


/// Use to mark an "abstract" function.
///
/// If executed, will cause the program to abort.
///
/// - Parameters:
///   - object: The object on in which the function is called
///   - file: The file reported - defaults to the file in which the function is called
///   - line: The line reported - defaults to the line on which the function is called
///   - function: The function that needed to be overridden - defaults to the currewnt function
/// - Returns: never
public func mustBeOverridden(_ object: AnyObject, file: String = #file, line: Int = #line, function: String = #function) -> Never
{
	fatalError("\(file)(\(line)): \(function) must be overridden in \(type(of: object))")
}

public extension Array
{
	/// Organise the elements of the array in groups
	///
	/// If the array count isn't evenly divisible by count, the last group will
	/// not contain the right number of elements.
	/// - Parameter groupSize: Size of each group
	/// - Returns: an array of arrays of elements each of which is the size of
	///            the group except possibly the last group.
	func  group(by groupSize: Int) -> [[Element]]
	{
		var ret: [[Element]] = []
		var group: [Element] = []
		for item in self
		{
			group.append(item)
			if group.count == groupSize
			{
				ret.append(group)
				group = []
			}
		}
		if !group.isEmpty
		{
			ret.append(group)
		}
		return ret
	}
}
