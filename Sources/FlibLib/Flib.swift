//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import ArmchairUniverseLib

/// Models a "Flib"
///
/// A Flib is a finite state machine that is designed to predict bit sequences.
/// Flibs have a chromosome that can be used to beed more Flibs.
class Flib
{
	private struct State
	{
		let nextStates: [Int]
		let outputs: [Int]

		init(out0: Int, state0: Int, out1: Int, state1: Int)
		{
			nextStates = [state0, state1]
			outputs = [out0, out1]
		}

		init?(allele: [Int])
		{
			guard allele.count >= 4 else { return nil }
			self.init(out0: allele[0], state0: allele[1], out1: allele[2], state1: allele[3])
		}
	}

	let chromosome: [Int]
	private let stateTable: [State]
	private var age = 0

	func predict(_ sequence: [Int]) -> [Int]
	{
		var stateIndex = 0
		var output: [Int] = []
		for input in sequence
		{
			let state = stateTable[stateIndex]
			stateIndex = state.nextStates[input % 2] % stateTable.count
			output.append(state.outputs[input % 2] % 2)
		}
		return output
	}

	/// Evaluate a sequence to see how good this flib is at predicting it.
	///
	/// Taking an input from the sequence we get an output from the flib that
	/// has to match the next input from the sequence. The last prediction, of
	/// course, is "off the end" of the sequence, so we ignore it.
	/// - Parameter sequence: The sequence to evaluate
	/// - Returns: A score representing the number of predictions it got right
	func evaluate(with sequence: [Int]) -> Int
	{
		let predictions = self.predict(sequence)
		let evaluationPairs = zip(predictions, sequence.dropFirst())
		let score = evaluationPairs.reduce(0)
		{
			$0 + $1.0 == $1.1 ? 1 : 0
		}
		return score
	}

	init(chromosome: [Int])
	{
		self.chromosome = chromosome
		let stateAlleles = chromosome.group(by: 4)
		stateTable = stateAlleles.compactMap { State(allele: $0) }
	}

	/// Breed a Flib with another flib
	/// - Parameters:
	///   - otherFlib: The other flib to breed with
	///   - rng: Ranom number generator used to select the crossover points
	/// - Returns: A new flib
	func breed<RNG: RandomNumberGenerator>(with otherFlib: Flib, using rng: inout RNG) -> Flib
	{
		let randomRange = 0 ..< chromosome.count
		let start = Int.random(in: randomRange, using: &rng)
		let end = Int.random(in: randomRange, using: &rng)
		let newChromosome = self.chromosome.cross(with: otherFlib.chromosome,
												  start: start,
												  end: end)
		return Flib(chromosome: newChromosome)
	}

	func makeOlder()
	{
		age += 1
	}
}

extension Array
{
	/// Cross an array with another array by splicing the other array into this
	/// one using the two indexes as plice points.
	///
	/// Note that the arrays must be the same length. Also, if `start` > `end`
	/// the splicing still starts from `start` but wraps around from the top to
	/// the bottom of the arrays.
	///
	/// If `start` and `end` are equal we return `self` which is a slight bias.
	/// - Parameters:
	///   - other: The other array to cross with
	///   - start: Start point. The index of the firstelement from `other`
	///   - end: end point. The index of the first element from `self` after
	///          copying in elements from `other`
	/// - Returns: The crossed over array
	func cross(with other: [Element], start: Int, end: Int) -> [Element]
	{
		precondition(self.count == other.count)

		// Deal with the equal case
		guard start != end else { return self }

		// Deal with the case where `start` > `end` by swapping over the arrays
		// and the indexes.
		guard start < end else { return other.cross(with: self, start: end, end: start) }

		let result = Array(self[0 ..< start]) + other[start ..< end] + self[end ..< self.count]
		return result
	}
}
