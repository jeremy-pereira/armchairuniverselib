//
//  Simulation.swift
//  
//
//  Created by Jeremy Pereira on 12/08/2020.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
import Statistics

/// Runs a Flib simulation
public struct Simulation
{
	private var sequence: [Int]
	private var population: [Flib] = []
	private var stats: Statistics<Evaluation>
	private var iteration = 0

	public init<S: Sequence>(statistics: String, sequence: S) throws
		where S.Element == Int
	{
		self.sequence = Array(sequence)
		self.stats = try Statistics(sqlite3File: statistics)
	}

	/// Create a new population using a specific random number genertior
	/// - Parameters:
	///   - count: Number of flibs in te population
	///   - stateCount: The number of states for the Fllib
	///   - rng: The random number generator to use
	public mutating func createPopulation<RNG: RandomNumberGenerator>(count: Int,
																	  stateCount: Int,
																	  using rng: inout RNG)
	{
		population = (0 ..< count).map
		{
			_ in
			let chromosome = (0 ..< stateCount).flatMap
			{
				_ in
				// There are two outputs and two next states per state
				return [
					Int.random(in: 0 ... 1, using: &rng),
					Int.random(in: 0 ..< stateCount, using: &rng),
					Int.random(in: 0 ... 1, using: &rng),
					Int.random(in: 0 ..< stateCount, using: &rng),
				]
			}
			return Flib(chromosome: chromosome)
		}
		iteration = 0
	}
	/// One iteration of the simulation
	/// - Throws: If we can't record the stats
	public mutating func iterate() throws
	{
		let evaluations = evaluateFlibs(sequence: sequence).sorted
		{
			$0.score < $1.score
		}
		try evaluations.forEach { try stats.record(dataPoint: $0) }
		population = evaluations.map({ $0.flib })

		iteration += 1
		ageFlibs()
	}

	private func evaluateFlibs(sequence: [Int]) -> [Evaluation]
	{
		var dataPoints: [Evaluation] = []
		for flib in population
		{
			dataPoints.append(Evaluation(iteration: iteration, score: flib.evaluate(with: sequence), flib: flib))
		}
		return dataPoints
	}

	private func ageFlibs()
	{
		population.forEach { $0.makeOlder() }
	}
}

fileprivate extension Simulation
{
	struct Evaluation: Recordable
	{
		let flib: Flib
		let score: Int
		let iteration: Int

		init(iteration: Int, score: Int, flib: Flib)
		{
			self.flib = flib
			self.score = score
			self.iteration = iteration
		}
		var data: [String : StatsMappable]
		{
			[
				"iteration" : iteration,
				"score" : score,
			]
		}
	}
}
