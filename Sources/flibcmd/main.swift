//
//  main.swift
//  
//
//  Created by Jeremy Pereira on 12/08/2020.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import FlibLib
import ArmchairUniverseLib
import ArgumentParser


struct FlibCmd: ParsableCommand
{
	@Option(help: "File to put the statistics in")
	var statsFile: String

	@Option(help: "Number of simulation iterations to run")
	var iterations: Int = 10

	func run() throws
	{
		let baseSequence = (0 ..< 6).map{ _ in  Int.random(in: 0...1) }
		let sequence = RepeatingSequence(maxRepeats: 10, array: baseSequence)

		do
		{
			var simulation = try Simulation(statistics: statsFile, sequence: sequence)
			var rng = SystemRandomNumberGenerator()
			simulation.createPopulation(count: 100, stateCount: 4, using: &rng)
			print("Running in \(FileManager.default.currentDirectoryPath) for \(iterations) iteration(s)")
			for _ in 0 ..< iterations
			{
				try simulation.iterate()
			}
		}
		catch
		{
			print("Failed to run simulation: \(error)")
		}

	}
}

FlibCmd.main()


