// swift-tools-version:5.2
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "ArmchairUniverseLib",
    platforms: [
        .macOS(.v10_15),
    ],
    products: [
        // Products define the executables and libraries produced by a package, and make them visible to other packages.
        .library(
            name: "ArmchairUniverseLib",
            targets: ["ArmchairUniverseLib"]),
        .library(
            name: "FlibLib",
            targets: ["FlibLib"]),
		.executable(name: "flibcmd", targets: ["flibcmd"]),
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
        // .package(url: /* package url */, from: "1.0.0"),
		.package(url: "https://github.com/apple/swift-argument-parser", from: "0.0.1"),
		.package(name: "Statistics", url: "https://jeremy-pereira@bitbucket.org/jeremy-pereira/statistics.git", from: "2.4.1"),
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages which this package depends on.
        .target(
            name: "ArmchairUniverseLib",
            dependencies: []),
        .target(
            name: "flibcmd",
			dependencies: ["FlibLib",
						   "ArmchairUniverseLib",
						   .product(name: "ArgumentParser", package: "swift-argument-parser")]),

        .testTarget(
            name: "ArmchairUniverseLibTests",
            dependencies: ["ArmchairUniverseLib"]),
	.target(
		name: "FlibLib",
		dependencies: ["ArmchairUniverseLib", .product(name: "Statistics", package: "Statistics")]),
	.testTarget(
		name: "FlibLibTests",
		dependencies: ["FlibLib"]),
    ]
)
